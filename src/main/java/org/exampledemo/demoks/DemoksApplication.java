package org.exampledemo.demoks;

import org.json.simple.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class DemoksApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoksApplication.class, args);
        new DemoksApplication().getData();
    }

    public void getData() {
        String url = "http://139.5.71.109:1906/api/NchlIPS/GetTxnDetailByBatchId";

        JSONObject stringStringMap = new JSONObject();
        stringStringMap.put("batchKey", "7677-00132");
        stringStringMap.put("amount", "10000.00");

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic TkNITDpuY2hsMTIz");
        httpHeaders.add("Content-Type", "application/json");
        HttpEntity<String> request =
                new HttpEntity<String>(stringStringMap.toString(), httpHeaders);
        ResponseEntity<String> data = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        System.out.println(data.getBody());
    }


}
